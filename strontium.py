# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

# An array of N words is given. Each word consists of small letters ('a' − 'z'). Our goal is to concatenate the words in such a way as to obtain a single word with the longest possible substring composed of one particular letter. Find the length of such a substring.
#
# Write a function:
#
# def solution(words)
#
# that, given an array words containing N strings, returns the length of the longest substring of a word created as described above.
#
# Examples:
#
# 1. Given N=3 and words=["aabb", "aaaa", "bbab"], your function should return 6. One of the best concatenations is words[1] + words[0] + words[2] = "aaaaaabbbbab". The longest substring is composed of letter 'a' and its length is 6.
#
# 2. Given N=3 and words=["xxbxx", "xbx", "x"], your function should return 4. One of the best concatenations is words[0] + words[2] + words[1] = "xxbxxxxbx". The longest substring is composed of letter 'x' and its length is 4.
#
# 3. Given N=4 and words=["dd", "bb", "cc", "dd"], your function should return 4. One of the best concatenations is words[0] + words[3] + words[1] + words[2] = "ddddbbcc". The longest substring is composed of letter 'd' and its length is 4.
#
# Write an efficient algorithm for the following assumptions:
#
# N is an integer within the range [1..100,000];
# all the words are non−empty and consist only of lowercase letters (a−z);
# S denotes the sum of the lengths of words; S is an integer within the range [1..100,000].


def count_longest_run(word, letter):
    longest_run = 0
    current_run = 0
    for char in word:
        if char == letter:
            current_run += 1
            longest_run = current_run if current_run > longest_run else longest_run
        else:
            current_run = 0
    return longest_run


def make_run(run_len, index):
    return {'len': run_len, 'idx': index}


def _count_runs(words, indices, run_counter):
    longest_run = make_run(0, -1)
    second_longest_run = make_run(0, -1)
    for index in indices:
        # check the word, but only if it's longer than the longest run so far, otherwise
        # it's a waste of time
        current_word = words[index]
        current_run = run_counter(current_word)
        if current_run > longest_run['len']:
            second_longest_run = longest_run
            longest_run = make_run(current_run, index)
        elif current_run > second_longest_run['len']:
            second_longest_run = make_run(current_run, index)
    return [longest_run, second_longest_run]


def count_start_runs(char, words, indices, word_data_map):
    def count_run_at_start(word):
        run_length = 0
        char_idx = 0
        current_word_len = word_data_map[word][1]
        while char_idx < current_word_len and word[char_idx] == char:
            run_length += 1
            char_idx += 1
        return run_length

    return _count_runs(words, indices, count_run_at_start)


def count_end_runs(char, words, indices, word_data_map):
    def count_run_at_end(word):
        run_length = 0
        char_idx = word_data_map[word][1] - 1
        while char_idx >= 0 and word[char_idx] == char:
            run_length += 1
            char_idx -= 1
        return run_length

    return _count_runs(words, indices, count_run_at_end)


def calc_longest_for_char_due_to_concatenation(char, words, exclusive_indices, total_exclusive_len, words_without_char,
                                               word_data_map):
    index_set = set([x for x in range(len(words))])
    non_exclusive_indices = index_set.difference(exclusive_indices)
    # this next step removes those words which don't include this char
    # at all, to avoid even checking them
    candidate_indices = non_exclusive_indices.difference(words_without_char)
    top_two_end_counts = count_end_runs(char, words, candidate_indices, word_data_map)
    top_two_start_counts = count_start_runs(char, words, candidate_indices, word_data_map)
    # the possibilities for combinations of first and last are:
    # [ls1, ls2] x [le1, le2]
    # remembering to exclude combinations that have the same index.
    # It's worth noting that the following even works in the degenerate case
    # where removing the exclusive_indices has left either 0 or 1 words to
    # be the prefixes and suffixes. It works because we only care about the cardinality
    # of the result and the cardinality of those degenerate cases is 0 anyway.
    max_cardinality_of_cart_prod = 0
    for i in range(len(top_two_end_counts)):
        for j in range(len(top_two_start_counts)):
            combination = [top_two_end_counts[i], top_two_start_counts[j]]
            if combination[0]['idx'] != combination[1]['idx']:
                max_cardinality_of_cart_prod = max(max_cardinality_of_cart_prod,
                                                   combination[0]['len'] + combination[1]['len'])
    return max_cardinality_of_cart_prod + total_exclusive_len


def categorise_words(words, char_set, word_data_map):
    '''
    Given a list of words and a char_set, this categorises
    them according to whether they are exclusively made up
    of a single char or whether they don't contain any of
    the given char. As a happy byproduct, we calculate
    the longest run of any char out of all the words
    considered individually.
    :param word_data_map:
    :param words:
    :param char_set:
    :return: A tuple (longest_run, exclusively_one_char_words_map, exclusives_total_len, words_without_char_map)
    '''
    # start by calculating the longest_run in each string
    # and identifying any strings that are exclusively
    # the given string
    exclusively_one_char_wordlists_map = {k: [] for k in char_set}
    exclusives_total_len = {k: 0 for k in char_set}
    words_without_char_lists_map = {k: [] for k in char_set}
    longest_run = 0
    for idx, word in enumerate(words):
        word_data = word_data_map[word]
        word_len = word_data[1]
        word_char_set = word_data[0]
        for char in char_set:
            if char not in word_char_set:
                words_without_char_lists_map[char].append(idx)
            else:
                longest_for_char = count_longest_run(word, char)
                if longest_for_char == word_len:
                    # this word is composed entirely of the given
                    # char so we record it got later
                    exclusively_one_char_wordlists_map[char].append(idx)
                    exclusives_total_len[char] += word_len
                longest_run = longest_for_char if longest_for_char > longest_run else longest_run

    # now convert the lists above to sets to make later operations
    # more efficient
    exclusively_one_char_words_map = {k: set(v) for k, v in exclusively_one_char_wordlists_map.items()}
    words_without_char_map = {k: set(v) for k, v in words_without_char_lists_map.items()}
    return longest_run, exclusively_one_char_words_map, exclusives_total_len, words_without_char_map


def get_char_set(words):
    '''
    :param words: a list of strings
    :return: a tuple of (the set of chars for all words, map of the set of chars per word)
    '''
    result = set()
    word_data_map = {}
    for word in words:
        word_char_set = set(word)
        word_data_map[word] = (word_char_set, len(word))
        result.update(word_char_set)
    return result, word_data_map


def cartesian_product(words):
    char_set, word_data_map = get_char_set(words)
    longest_run, exclusively_one_char_words_map, exclusives_total_len, words_without_char_map = \
        categorise_words(words, char_set, word_data_map)

    # now we can focus, ONE LETTER AT A TIME, on counting the connected
    # runs between words. Since we counted the longest run out of individual
    # words above we can now consider 3 types of words from the perspective
    # of a specific letter:
    # (a) words that end with the letter
    # (b) words that are entirely composed of the letter
    # (c) words that begin with the letter
    #
    # (b) category words are removed first. (a) and (c) are still intersecting
    # sets - consider the word "aba", for example. We only want to find the
    # cardinality of the cartesian product of these sets, which reduces
    # pretty simply to:
    # (A) [longest run at end, 2nd longest run at end] x
    # (B) [words composed only of the letter] x
    # (C) [longest run at start, 2nd longest run at start]
    # The reason for needing to include both the longest and 2nd longest runs
    # is the possibility that one word has both the longest run at the start
    # and the end and since it will get used up on one of the combinations it
    # isn't available for the other end of the product (i.e., start or end).
    # The product we're looking for is then:
    # sum(len(B)) + max(A X C)
    # where A X C is the cartesian product, being careful to not use the same
    # word twice.
    # The time complexity of this approach is O(S) where S is the total length
    # of the strings. Note that this is per character type, but since this is
    # a constant max of 26 then the complexity is still O(S)

    for char in char_set:
        longest_for_char = calc_longest_for_char_due_to_concatenation(char, words, exclusively_one_char_words_map[char],
                                                                      exclusives_total_len[char],
                                                                      words_without_char_map[char], word_data_map)
        longest_run = max(longest_run, longest_for_char)

    return longest_run


def solution(words):
    return cartesian_product(words)
