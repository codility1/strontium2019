import itertools

from common import valid_chars


def chars_of(strings, indices):
    for index in indices:
        curr_string = strings[index]
        for char in curr_string:
            yield char


def count_longest_run(strings, indices, letter):
    longest_run = 0
    current_run = 0
    for char in chars_of(strings, indices):
        if char == letter:
            current_run += 1
            longest_run = max(current_run, longest_run)
        else:
            current_run = 0
    return longest_run


def brute_force(words):
    list_indices = [x for x in range(len(words))]
    longest_run = 0
    for indices in itertools.permutations(list_indices):
        for char in valid_chars:
            curr_longest_run = count_longest_run(words, indices, char)
            longest_run = max(curr_longest_run, longest_run)
    return longest_run
