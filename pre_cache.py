import itertools


def chars_of(strings, indices):
    for index in indices:
        curr_string = strings[index]
        for char in curr_string:
            yield char


def count_longest_run(strings, indices, letter, total_len, longest_run_so_far):
    longest_run = 0
    current_run = 0
    chars_remaining = total_len
    for char in chars_of(strings, indices):
        if char == letter:
            current_run += 1
            longest_run = max(current_run, longest_run)
        else:
            current_run = 0
        chars_remaining -= 1
        if longest_run + chars_remaining <= longest_run_so_far:
            # there's no point in continuing because even if
            # every word contains only the letter we're looking
            # for it still won't be enough to beat our overall
            # best so far
            return longest_run
    return longest_run


def extensive_search(words, char, longest_run_so_far):
    total_len = sum([len(x) for x in words])
    list_indices = [x for x in range(len(words))]
    longest_run = longest_run_so_far
    for indices in itertools.permutations(list_indices):
        curr_longest_run = count_longest_run(words, indices, char, total_len, longest_run)
        longest_run = max(curr_longest_run, longest_run)
    return longest_run


def get_char_set(words):
    '''
    :param words: a list of strings
    :return: the set of chars in the given words
    '''
    result = set()
    for word in words:
        result.update(word)
    return result


def pre_cache_candidates(words):
    char_set = get_char_set(words)
    # start by calculating the longest_run in each string
    longest_run = 0
    for word in words:
        for char in char_set:
            current_longest = extensive_search([word], char, longest_run)
            longest_run = max(longest_run, current_longest)

    # first gather the words into subsets that actually start or end with
    # a given letter so that the search space is much smaller. After all,
    # they can't all start or end with every letter!
    letter_map = {k: [] for k in char_set}
    for char in char_set:
        for word in words:
            if word.startswith(char) or word.endswith(char):
                letter_map[char].append(word)
    # now that we have these subsets we can brute force each one individually
    # note that in the worst case this could end up permuting every list still
    # and even multiple times. An early out will help here - if we know how
    # many chars are left in the current permutation we can early out if:
    # current_longest + chars_remaining < longest_so_far
    for char, subset in letter_map.items():
        current_longest = extensive_search(subset, char, longest_run)
        longest_run = max(longest_run, current_longest)
    return longest_run
