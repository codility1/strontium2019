validChars = [chr(i) for i in range(ord('a'), ord('z') + 1)]


def preprocess(words):
    prefixes = []
    suffixes = []
    singleCharTypeTotalLength = {k: 0 for k in validChars}
    longestRunInSingleWord = 0
    for word in words:
        previousChar = None
        count = 0
        for char in word:
            if char == previousChar:
                count += 1
            else:
                longestRunInSingleWord = max(count + 1, longestRunInSingleWord)
                count = 0
            previousChar = char

        prefixLength = 0
        for char in word:
            if char != word[0]:
                prefixes.append([word[0], prefixLength])
                break
            prefixLength += 1

        if prefixLength == len(word):
            # in this case nothing will have been appended to prefixes
            singleCharTypeTotalLength[word[0]] += prefixLength
        else:
            suffixLength = 0
            for char in word[::-1]:
                if char != word[-1]:
                    suffixes.append([word[-1], suffixLength])
                    break
                suffixLength += 1
    return prefixes, suffixes, singleCharTypeTotalLength, longestRunInSingleWord


def c_style_refactored(words):
    prefixes, suffixes, singleCharTypeTotalLength, longestRunInSingleWord = preprocess(words)

    prefixMax = {k: 0 for k in validChars}
    suffixMax = {k: 0 for k in validChars}
    maxForCharType = {k: 0 for k in validChars}
    for i in range(len(prefixes)):
        suffixChar = suffixes[i][0]
        suffixCharCount = suffixes[i][1]
        maxForCharType[suffixChar] = max(maxForCharType[suffixChar], suffixCharCount + prefixMax[suffixChar])

        prefixChar = prefixes[i][0]
        prefixCharCount = prefixes[i][1]
        maxForCharType[prefixChar] = max(maxForCharType[prefixChar], prefixCharCount + suffixMax[prefixChar])

        suffixMax[suffixChar] = max(suffixMax[suffixChar], suffixCharCount)
        prefixMax[prefixChar] = max(prefixMax[prefixChar], prefixCharCount)

    comboMaxes = [(maxForCharType[k] + singleCharTypeTotalLength[k]) for k in maxForCharType]
    return max(longestRunInSingleWord, max(comboMaxes))
