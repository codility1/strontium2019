import random

valid_chars = [chr(i) for i in range(ord('a'), ord('z') + 1)]
MAX_NUM_STRINGS = 100000
MAX_TOTAL_LEN = 100000


def generate_random_word(str_size, char_set):
    random_word = ''
    for _ in range(str_size):
        index = random.randrange(len(char_set))
        random_word += char_set[index]
    return random_word


def generate_large_random(char_set, num_strings=10, total_len=100):
    remaining_len = total_len
    words = []
    str_size = total_len // num_strings
    for _ in range(num_strings):
        random_size_offset = random.randrange(-str_size // 4, +str_size // 4)
        word_size = max(1, str_size + random_size_offset)
        random_word = generate_random_word(word_size, char_set)
        words.append(random_word)
        remaining_len -= str_size
    if remaining_len > 0:
        random_word = generate_random_word(remaining_len, char_set)
        words[0] += random_word
    return words
