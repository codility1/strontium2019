from itertools import permutations, groupby


def ultra_pythonic(words):
    max_sequence_len = 0
    for word_combo in permutations(words):
        concatenated_combo = ''.join(word_combo)
        longest_sequence_in_combo = max([sum(1 for _ in group) for _, group in groupby(concatenated_combo)])
        max_sequence_len = max(max_sequence_len, longest_sequence_in_combo)
    return max_sequence_len
