from itertools import groupby, product

valid_chars = [chr(i) for i in range(ord('a'), ord('z') + 1)]


def compare_and_insert(group, affix_list):
    if group[1] > affix_list[0][1]:
        affix_list[1] = affix_list[0]
        affix_list[0] = group
    elif group[1] > affix_list[1][1]:
        affix_list[1] = group


def terse_pythonic(words):
    top_two_prefixes = {k: [(-1, 0), (-1, 0)] for k in valid_chars}
    top_two_suffixes = {k: [(-1, 0), (-1, 0)] for k in valid_chars}
    total_length_of_single_char_type_words = {k: 0 for k in valid_chars}
    longest_run_in_any_word = 0

    for index, word in enumerate(words):
        letter_group_lens = [sum(1 for _ in group) for _, group in groupby(word)]
        if len(letter_group_lens) == 1:
            total_length_of_single_char_type_words[word[0]] += letter_group_lens[0]
        else:
            compare_and_insert((index, letter_group_lens[0]), top_two_prefixes[word[0]])
            compare_and_insert((index, letter_group_lens[-1]), top_two_suffixes[word[-1]])
            longest_run_in_any_word = max(longest_run_in_any_word, max(letter_group_lens))

    combination_max = 0
    for char in valid_chars:
        max_affix_combo = 0
        for combo in product(top_two_suffixes[char], top_two_prefixes[char]):
            if combo[0][0] != combo[1][0]:  # different indices
                max_affix_combo = max(max_affix_combo, combo[0][1] + combo[1][1])
        combination_max = max(combination_max, max_affix_combo + total_length_of_single_char_type_words[char])

    return max(longest_run_in_any_word, combination_max)
