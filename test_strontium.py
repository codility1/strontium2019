import unittest

from c_style_refactored import c_style_refactored
from common import generate_random_word, generate_large_random, valid_chars, MAX_NUM_STRINGS, MAX_TOTAL_LEN
from pythonic import pythonic
from semi_pythonic import semi_pythonic
from strontium import solution
from pre_cache import pre_cache_candidates
from brute_force import brute_force
import random
from collections import Counter

from c_style import c_style
from terse_pythonic import terse_pythonic
from ultra_pythonic import ultra_pythonic


class StrontiumTestCase(unittest.TestCase):

    def setUp(self):
        random.seed(129)  # so random

    def _test_given_cases_for(self, solver):
        self.assertEqual(3, solver(["aba", "bab", "b", "bab", "ba"]))
        self.assertEqual(7, solver(["aabb", "aaaa", "bbab", "ac", "a"]))
        self.assertEqual(6, solver(["aabb", "aaaa", "bbab"]))
        self.assertEqual(4, solver(["xxbxx", "xbx", "x"]))
        self.assertEqual(4, solver(["dd", "bb", "cc", "dd"]))

    def double_check(self, words, expected):
        if len(words) < 100:
            # this one's just too damn slow otherwise
            self.assertEqual(expected, pre_cache_candidates(words))
            self.assertEqual(expected, ultra_pythonic(words))
        self.assertEqual(expected, solution(words))
        self.assertEqual(expected, pythonic(words))
        self.assertEqual(expected, semi_pythonic(words))
        self.assertEqual(expected, terse_pythonic(words))

    def _check_against_known_correct(self, words):
        expected = solution(words)
        self.assertEqual(expected, c_style(words))
        self.assertEqual(expected, c_style_refactored(words))
        self.assertEqual(expected, pythonic(words))
        self.assertEqual(expected, semi_pythonic(words))
        self.assertEqual(expected, terse_pythonic(words))

    def test_given_cases(self):
        self._test_given_cases_for(solution)
        self._test_given_cases_for(c_style)
        self._test_given_cases_for(c_style_refactored)
        self._test_given_cases_for(pythonic)
        self._test_given_cases_for(semi_pythonic)
        self._test_given_cases_for(terse_pythonic)
        self._test_given_cases_for(ultra_pythonic)

    def test_small_simple(self):
        self.assertEqual(3, solution(['abbba']))
        self.assertEqual(3, c_style(['abbba']))
        self.assertEqual(3, c_style_refactored(['abbba']))
        self.assertEqual(3, pythonic(['abbba']))
        self.assertEqual(3, semi_pythonic(['abbba']))
        self.assertEqual(3, terse_pythonic(['abbba']))
        self.assertEqual(3, ultra_pythonic(['abbba']))

    def test_largest_prefix_and_suffix_in_one_word(self):
        words = ['{0}b{0}'.format('a' * 7), 'xyzabcq', '{0}b{0}'.format('a' * 6), 'a' * 11, 'b' * 13]
        self.double_check(words, 24)

    def test_largest_prefix_and_suffix_in_only_one_word(self):
        words = ['{0}b{0}'.format('a' * 7), 'xyzabcq', 'f{0}b{0}f'.format('a' * 6), 'a' * 11, 'b' * 13]
        self.double_check(words, 18)

    def test_different_largest_prefix_and_suffix_in_one_word(self):
        words = ['{0}b{1}'.format('a' * 7, 'c' * 7), 'xyzabcq', '{0}b{0}'.format('a' * 6), 'a' * 11, 'c' * 13]
        self.double_check(words, 24)

    def test_differently_sized_largest_prefix_and_suffix_in_one_word(self):
        words = ['{0}b{1}'.format('a' * 7, 'a' * 8), 'xyzabcq', 'a' * 11, 'c' * 13]
        self.double_check(words, 19)

    def test_diff_char_but_equal_max_prefix_suffix(self):
        words = ['{0}{1}'.format('a' * 8, 'b' * 8), 'xyzabcq' * 6, '{}'.format('a' * 10), '{}'.format('b' * 11)]
        self.double_check(words, 19)

    def test_different_largest_prefix_and_suffix_in_only_one_word(self):
        words = ['{0}b{1}'.format('a' * 7, 'c' * 7), 'xyzabcq', 'f{0}b{0}f'.format('a' * 6), 'a' * 11, 'c' * 13]
        self.double_check(words, 20)

    def test_multiple_words_with_largest_prefix_and_suffix(self):
        words = ['{0}b{0}'.format('a' * 7), '{0}b{0}'.format('c' * 7), 'xyzabcq',
                 'f{0}b{0}f'.format('a' * 19), 'a' * 11, 'c' * 12]
        self.double_check(words, 19)

    def test_repeated_best_prefix_and_suffix_in_same_word(self):
        words = ['aaabaa', 'aaabaa', 'aba', 'aa']
        expected = 7
        self.double_check(words, expected)
        self.assertEqual(expected, c_style(words))

    def test_single_best_prefix_and_suffix(self):
        words = ['aaabaa', 'aba', 'aa']
        expected = 6
        self.double_check(words, expected)
        self.assertEqual(expected, c_style(words))

    def test_prefix_suffix_same_word(self):
        words = ['xxxxxxxxxxxxx', 'xxx', 'zoozxzoxoo', 'xozxozoxx', 'xxxxxoxxxxx', 'xoxo']
        self.double_check(words, 23)

    def test_medium_random(self):
        for _ in range(1):
            words = generate_large_random(valid_chars, num_strings=10, total_len=100)
            self._check_against_known_correct(words)


    def test_medium_random_from_small_set_of_chars(self):
        small_char_set = ['a', 'b']
        for _ in range(1):
            words = generate_large_random(small_char_set, num_strings=10, total_len=100)
            self._check_against_known_correct(words)

    def test_large_random(self):
        for _ in range(1):
            words = generate_large_random(valid_chars, num_strings=10, total_len=10000)
            self._check_against_known_correct(words)

    def test_large_random_from_small_set_of_chars(self):
        small_char_set = ['a', 'b']
        for _ in range(1):
            words = generate_large_random(small_char_set, num_strings=10, total_len=10000)
            self._check_against_known_correct(words)

    def test_one_max_len_word(self):
        small_char_set = ['c', 'v']
        words = [generate_random_word(MAX_NUM_STRINGS, small_char_set)]
        self._check_against_known_correct(words)

    def test_max_len_one_char(self):
        words = ['a' * MAX_TOTAL_LEN]
        self._check_against_known_correct(words)

    def test_max_num_strings(self):
        words = generate_large_random(char_set=valid_chars, num_strings=MAX_NUM_STRINGS, total_len=MAX_TOTAL_LEN)
        letter_counts = Counter(words)
        expected = max(letter_counts.values())
        actual = solution(words)
        self.assertEqual(expected, actual)
        self.assertEqual(expected, pythonic(words))


if __name__ == '__main__':
    unittest.main()
