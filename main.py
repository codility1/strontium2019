import random
import cProfile
import pstats

from c_style_refactored import c_style_refactored
from pythonic import pythonic
from semi_pythonic import semi_pythonic
from strontium import solution
from common import valid_chars, generate_large_random, MAX_TOTAL_LEN, MAX_NUM_STRINGS
from c_style import c_style
from terse_pythonic import terse_pythonic


def run_hard(words, function_to_profile):
    for _ in range(10):
        function_to_profile(words)


def profile(words, function_to_profile):
    print('Num words = {}'.format(len(words)))
    pr = cProfile.Profile()
    pr.enable()
    run_hard(words, function_to_profile)
    pr.disable()
    pstats.Stats(pr).sort_stats('time', 'cumulative').print_stats()


def main():
    random.seed(129)  # so random
    functions_to_profile = [solution, c_style, c_style_refactored, pythonic, semi_pythonic, terse_pythonic]
    for function_to_profile in functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing large set of randomly sized words:')
        words = generate_large_random(char_set=valid_chars, num_strings=MAX_TOTAL_LEN // 10, total_len=MAX_TOTAL_LEN)
        profile(words, function_to_profile)

        print('Testing max sized set of single letter words:')
        words = generate_large_random(char_set=valid_chars, num_strings=MAX_NUM_STRINGS, total_len=MAX_TOTAL_LEN)
        profile(words, function_to_profile)


if __name__ == '__main__':
    main()
