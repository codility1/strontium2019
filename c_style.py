def preprocess(words):
    # write your code in Python 3.6
    fs = []
    bs = []
    alls = {}
    mx = 0
    for w in words:
        pre = None
        cnt = 0
        for c in w:
            if c == pre:
                cnt += 1
            else:
                mx = max(cnt + 1, mx)
                cnt = 0
            pre = c

        f = 0
        for c in w:
            if c != w[0]:
                fs.append([w[0], f])
                break
            f += 1

        if f == len(w):
            if w[0] in alls:
                alls[w[0]] += f
            else:
                alls[w[0]] = f
        else:
            b = 0
            for c in w[::-1]:
                if c != w[-1]:
                    bs.append([w[-1], b])
                    break
                b += 1
    return fs, bs, alls, mx


def c_style(words):
    fs, bs, alls, mx = preprocess(words)

    fmx = {}
    bmx = {}
    allmx = {}
    for i in range(len(fs)):
        cb = bs[i][0]
        bc = bs[i][1]
        if cb in fmx:
            bc += fmx[cb]
        if cb not in allmx or bc > allmx[cb]:
            allmx[cb] = bc

        cf = fs[i][0]
        fc = fs[i][1]
        if cf in bmx:
            fc += bmx[cf]
        if cf not in allmx or fc > allmx[cf]:
            allmx[cf] = fc

        if cb not in bmx or bs[i][1] > bmx[cb]:
            bmx[cb] = bs[i][1]
        if cf not in fmx or fs[i][1] > fmx[cf]:
            fmx[cf] = fs[i][1]

    for k in alls:
        if k in allmx:
            allmx[k] += alls[k]
        else:
            allmx[k] = alls[k]

    return max(mx, max(allmx.values()))
