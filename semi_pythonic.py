from itertools import groupby, product

valid_chars = [chr(i) for i in range(ord('a'), ord('z') + 1)]


def insert_sorted(two_element_list, affix_tuple):
    affix_len = affix_tuple[1]
    if affix_len > two_element_list[0][1]:
        two_element_list.insert(0, affix_tuple)
    elif affix_len > two_element_list[1][1]:
        two_element_list[1] = affix_tuple


def process_word(word, single_letter_words_total_len):
    prefix_len = -1
    suffix_len = -1
    max_mid_string_len = 0
    word_len = len(word)
    current_run_count = 1
    prefix_char = word[0]
    current_char_idx = 0
    next_char_idx = 1
    while current_char_idx < word_len:
        current_char = word[current_char_idx]
        if next_char_idx < word_len:
            next_char = word[next_char_idx]
            if current_char != next_char:
                if prefix_len == -1:
                    prefix_len = current_run_count
                else:
                    max_mid_string_len = max(max_mid_string_len, current_run_count)
                current_run_count = 1
            else:
                current_run_count += 1
        else:
            if current_run_count == word_len:
                # single letter word
                single_letter_words_total_len[prefix_char] += word_len
                prefix_len = -1
                suffix_len = -1
            else:
                suffix_len = current_run_count
        current_char_idx += 1
        next_char_idx += 1
    return prefix_len, suffix_len, max_mid_string_len


def process_words(words):
    top_prefix_lens = {k: [(-1, 0), (-1, 0)] for k in valid_chars}
    top_suffix_lens = {k: [(-1, 0), (-1, 0)] for k in valid_chars}
    single_letter_words_total_len = {k: 0 for k in valid_chars}
    max_single_word_run = 0
    for idx, word in enumerate(words):
        prefix_len, suffix_len, max_mid_string_len = process_word(word, single_letter_words_total_len)
        if suffix_len != -1:
            # this word is made up of at least 2 letter types (otherwise
            # it would be a single letter word as above) so the length
            # is >= 2.
            # First we get the prefix and suffix lengths and store them
            # into a queue with only 2 elements, sorted by length
            # This maintains the top two lengths for prefix and suffix.
            prefix_char = word[0]
            insert_sorted(top_prefix_lens[prefix_char], (idx, prefix_len))
            suffix_char = word[-1]
            insert_sorted(top_suffix_lens[suffix_char], (idx, suffix_len))
            max_single_word_run = max(max_single_word_run, max_mid_string_len)
    return top_prefix_lens, top_suffix_lens, single_letter_words_total_len, max_single_word_run


def semi_pythonic(words):
    top_prefix_lens, top_suffix_lens, single_letter_words_total_len, max_single_word_run = process_words(words)

    # there are now two possibilities - either the longest run was in a single word with no concatenation
    # possible OR we need to concatenate. At this stage, max_single_word_run
    # contains the longest run within a word, so we calculate the longest concatenation
    max_concatenation = 0
    for char in valid_chars:
        # the top affix len maps contain the top two lengths for that affix type,
        # along with the index of the word it occurred in. We can use this to
        # ensure we don't use the same word for both a prefix and a suffix
        max_affix_len = 0
        # we slice the following for the top two suffixes - these are the only positions
        # that we maintain as sorted
        for affix_pair in product(top_suffix_lens[char][:2], top_prefix_lens[char][:2]):
            suffix = affix_pair[0]
            prefix = affix_pair[1]
            if suffix[0] != prefix[0]:  # different indices
                max_affix_len = max(max_affix_len, suffix[1] + prefix[1])
        max_concatenation_length_for_char = max_affix_len + single_letter_words_total_len[char]
        max_concatenation = max(max_concatenation, max_concatenation_length_for_char)

    return max(max_single_word_run, max_concatenation)
