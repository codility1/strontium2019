#include <algorithm>
#include <unordered_map>
#include <list>

struct Group
{
    Group(const char c, const int i) : character(c), count(i) {}
    char character;
    int count;
};

struct AffixInfo
{
    AffixInfo(const Group _group, const int _indexOfWord) : group(_group), indexOfWord(_indexOfWord) {}
    Group group;
    int indexOfWord;
};

void groupBy(const string& word, vector<Group>& groups)
{
    char currentChar = word[0];
    int currentCount = 1;
    for(int i=1; i < (int)word.length(); ++i)
    {
        if(word[i] != currentChar)
        {
            groups.push_back(Group(currentChar, currentCount));
            currentChar = word[i];
            currentCount = 1;
        }
        else
        {
            currentCount++;
        }
    }
    groups.push_back(Group(currentChar, currentCount));
}

unordered_map<char, int> initCharTypeCounts()
{
    unordered_map<char, int> singleCharTypeCounts;
    for (char c = 'a'; c <= 'z'; ++c)
    {
        singleCharTypeCounts[c] = 0;
    }
    return singleCharTypeCounts;
}

list<AffixInfo> createTopTwoList(const char c)
{
    list<AffixInfo> topTwoList;
    topTwoList.push_back(AffixInfo(Group(c, 0), -1));
    topTwoList.push_back(AffixInfo(Group(c, 0), -1));
    return topTwoList;
}

unordered_map<char, list<AffixInfo>> createTopTwoLists()
{
    unordered_map<char, list<AffixInfo>> topTwoLists;
    for (char c = 'a'; c <= 'z'; ++c)
    {
        topTwoLists[c] = createTopTwoList(c);
    }
    return topTwoLists;
}

void compareAndInsert(const AffixInfo& affixInfo, list<AffixInfo>& topTwoAffixes)
{
    if (affixInfo.group.count > topTwoAffixes.front().group.count)
    {
        topTwoAffixes.push_front(affixInfo);
        topTwoAffixes.pop_back(); // keep the group size at two
    }
    else if (affixInfo.group.count > topTwoAffixes.back().group.count)
    {
        topTwoAffixes.pop_back();
        topTwoAffixes.push_back(affixInfo);
    }
}

int solution(vector<string>& words) {

    unordered_map<char, int> singleCharTypeCounts = initCharTypeCounts();
    unordered_map<char, list<AffixInfo>> topTwoSuffixes = createTopTwoLists();
    unordered_map<char, list<AffixInfo>> topTwoPrefixes = createTopTwoLists();
    int longestRunWithinAnyWord = 0;

    for(int i=0; i < (int)words.size(); ++i)
    {
        const string& word = words[i];
        const char firstLetter = word[0];
        vector<Group> groups;
        groupBy(word, groups);
        if (groups.size() == 1)
        {
            singleCharTypeCounts[firstLetter] += word.length();
        }
        else
        {
            // at least 2 groups
            const char prefixChar = groups[0].character;
            compareAndInsert(AffixInfo(groups[0], i), topTwoPrefixes[prefixChar]);
            const Group& suffixGroup = groups[groups.size() - 1];
            const char suffixChar = suffixGroup.character;
            compareAndInsert(AffixInfo(suffixGroup, i), topTwoSuffixes[suffixChar]);
            if (groups.size() > 2)
            {
                auto findLongestGroup = [](const Group& lhs, const Group& rhs){ return lhs.count > rhs.count; };
                const int longestRunWithinCurrentWord = max_element(groups.begin(), groups.end(), findLongestGroup)->count;
                longestRunWithinAnyWord = max(longestRunWithinAnyWord, longestRunWithinCurrentWord);
            }
        }
    }

    int maxCombination = 0;
    for (const auto singleCharTypeCountPair : singleCharTypeCounts)
    {
        // check each combination of the prefixes and suffixes to get the maximal combination
        const char currentChar = singleCharTypeCountPair.first;
        int maxAffixes = 0;
        for(const AffixInfo& suffix : topTwoSuffixes[currentChar])
        {
            for(const AffixInfo& prefix : topTwoPrefixes[currentChar])
            {
                if(suffix.indexOfWord != prefix.indexOfWord)  // i.e., they're not the same word
                {
                    maxAffixes = max(maxAffixes, suffix.group.count + prefix.group.count);
                }
            }
        }
        maxCombination = max(maxCombination, maxAffixes + singleCharTypeCountPair.second);
    }

    return max(longestRunWithinAnyWord, maxCombination);
}